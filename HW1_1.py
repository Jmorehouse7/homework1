import numpy as np

def generate_y(phi,sigma,y_,T,epsilon):
    r'''
    Generates :math:`y_t` following 
        
        :math:`y_t = \phi_0+\phi_1y_{t-1}+\phi_2 y_{t-2} + \sigma \epsilon_t`

    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:`\sigma`
    
    * y_ (list or array) : :math:`[y_{-1},y_{-2}]`
    
    * T (int) : number of periods to simulate
    
    * epsilon (length T+1 numpy array) : :math:`[\epsilon_0,\epsilon_1,...,\epsilon_T]`
    
    
    
    Returns
    -----------
    * y(length T+1 numpy array) : :math:`[y_0,y_1,...,y_T]`
    '''
    pass

def generate_N_y(phi,sigma,y_,N,T):
    r'''
    Generates N realizations of y^i_t following 

        :math:`y^i_t = \phi_0+\phi_1y^i_{t-1}+\phi_2 y^i_{t-2} + \sigma \epsilon^i_t`
    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:`\sigma`
    
    * y_ (list or array) : :math:`[y_{-1},y_{-2}]`
    
    * N (int) : number of simulations
    
    * T (int) : number of periods to simulate
    
    
    Returns
    -----------
    * y(Nx(T+1) numpy array) : y[i,t] is the realization of y at time t for simulation i
    '''
    pass

def generate_AC(phi,sigma):
    r'''
    Create A,C for the process
    
        :math:`x_{t} = A x_{t-1} + C\epsilon_{t}`
    
    Inputs
    -----------
    * phi (list or array) : [phi_0,phi_1,phi_2]
    
    * sigma (float) : :math:`\sigma`
    
    
    Returns
    -----------
    * A ( :math:`(3\times 3)` array)
    * C (length 3 array)
    '''
    pass

def generate_x(A,C,x_,T,epsilon):
    r'''
    Generates :math:`x_t` following 
        
        :math:`x_{t} = A x_{t-1} + C\epsilon_{t}`

    
    Inputs
    -----------
    * A (:math:`m\times m` array)
    
    * C (length m array)
    
    * x_ (length m array) : :math:`x_{-1}`
    
    * T (int) : number of periods to simulate
    
    * epsilon (length T+1 numpy array) : :math:`[\epsilon_0,\epsilon_1,...,\epsilon_T]`
    
    
    Returns
    -----------
    * x(length :math:`m\times(T+1) numpy array) : x[:,t] is the array :math:`x_t`
    '''
    pass

def generate_xhat(A,xhat_,T):
    r'''
    Generates :math:`\hat x_t` following 
        
        :math:`\hat x_{t} = A \hat x_{t-1}`

    
    Inputs
    -----------
    * A (:math:`m\times m` array)
    
    * xhat_ (length m array) : :math:`\hat x_{-1}`
    
    * T (int) : number of periods to simulate
    
    
    Returns
    -----------
    * xhat(length :math:`m\times(T+1) numpy array) : xhat[:,t] is the array 
    :math:`\hat x_t`
    '''
    pass




